bought_price = ARGV[0].to_f
sold_price = ARGV[1].to_f
existing_gain = ARGV[2].to_f || 0 
amount = ARGV[3]&.to_f || 200000

total_stocks = amount/bought_price
total_revenue = total_stocks * sold_price
net_gain = total_revenue - amount
net_gain_after_tax = net_gain * 0.7
india_towards_goal = net_gain_after_tax.round(2) * 72
india_remaining_in_inr = 9000000 - india_towards_goal
india_remaining_in_usd = india_remaining_in_inr/72 - existing_gain
num_months_on_3k_average_to_pay_off = india_remaining_in_usd/3000
puts "=================================="
puts "total revenue for #{amount}: #{net_gain.round(2)}"
puts "total revenue after tax: #{net_gain_after_tax.round(2)}"
#puts "India house goal left: #{india_remaining_in_usd.round(2)}"
#puts "num months to pay off on 3k per month average: #{num_months_on_3k_average_to_pay_off.floor}"
puts "=================================="
